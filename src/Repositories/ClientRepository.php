<?php

namespace Drupal\druxt_oauth\Repositories;

use Drupal\simple_oauth\Repositories\ClientRepository as ClientRepositoryBase;

/**
 * Custom oauth client repository, which skips some unnecessary access checks.
 */
class ClientRepository extends ClientRepositoryBase {

  /**
   * {@inheritdoc}
   */
  public function validateClient($client_identifier, $client_secret, $grant_type) {
    if (!$client_drupal_entity = $this->getClientDrupalEntity($client_identifier)) {
      return FALSE;
    }

    if (!$client_drupal_entity->get('confidential')->value &&
      empty($client_secret) &&
      $grant_type !== 'client_credentials') {
      return TRUE;
    }

    return parent::validateClient($client_identifier, $client_secret, $grant_type);
  }

}
