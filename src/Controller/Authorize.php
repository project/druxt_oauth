<?php

namespace Drupal\druxt_oauth\Controller;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\simple_oauth\Controller\Oauth2AuthorizeController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Custom authorize controller.
 */
class Authorize extends Oauth2AuthorizeController {

    /**
     * Authorize override to add redirects.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   The request.
     *
     * @return mixed|\Symfony\Component\HttpFoundation\RedirectResponse
     *   The response.
     */
    public function authorizeOverride(Request $request) {
        /** @var \Drupal\Core\Routing\TrustedRedirectResponse $response */
        $response = parent::authorize($request);
        if ($response instanceof TrustedRedirectResponse && $request->query->has('redirect')) {
            $target_url = $response->getTargetUrl(). '&redirect=' . $request->query->get('redirect');
            $response->setTrustedTargetUrl($target_url);
            $response->setTargetUrl($target_url);
        }
        return $response;
    }
}
