<?php

namespace Drupal\druxt_oauth;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Service provider for druxt oauth module.
 */
class DruxtOauthServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('simple_oauth.repositories.client')) {
      $definition = $container->getDefinition('simple_oauth.repositories.client');
      $definition->setClass('Drupal\druxt_oauth\Repositories\ClientRepository');
    }
  }
}
